#ifndef CUSTOM_TESTS_H
#define CUSTOM_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <stdio.h>

void test_all();
void test_simple_mem_alloc();
void test_alloc_blocks_free_one();
void test_alloc_blocks_free_two();
void test_new_region_extends_old();
void test_new_region_extends_old_another_place();

#endif
