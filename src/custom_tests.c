#include "custom_tests.h"

#define SUCCESS 1
#define FAILURE 1

#define HEAP_INIT (struct block_header*) heap_init(REGION_MIN_SIZE)
#define HEAP_DESTROY heap_destroy(REGION_MIN_SIZE, heap);
#define PRINT_DEBUG debug_heap(stdout, heap)

static void print_result(size_t test_num, bool result, char *msg) {
    printf("[TEST %zu | %s] %s\n", test_num, result ? "Passed" : "Failed", msg);
}

static void heap_destroy(size_t sz, void *heap) {
    munmap(
            heap,
            size_from_capacity(
                    (block_capacity) {sz}
            ).bytes
    );
}

/* --------------- TESTS --------------- */

void test_simple_mem_alloc() {
    struct block_header *heap = HEAP_INIT;

    if (heap == NULL) {
        print_result(1, FAILURE, "Could not initialize heap");
        return;
    }

    PRINT_DEBUG;

    void *mem = _malloc(512);

    if (mem == NULL) {
        print_result(1, FAILURE, "Could not allocate memory");
        HEAP_DESTROY;
        return;
    }

    PRINT_DEBUG;

    if (heap->is_free) {
        print_result(1, FAILURE, "Memory was allocated but heap is still free");
    }

    _free(mem);
    HEAP_DESTROY;
    print_result(1, SUCCESS, "OK");
}

void test_alloc_blocks_free_one() {
    void *heap = HEAP_INIT;

    if (heap == NULL) {
        print_result(2, FAILURE, "Could not initialize heap");
        return;
    }

    PRINT_DEBUG;

    void *fst_mem = _malloc(512);
    void *snd_mem = _malloc(512);
    void *trd_mem = _malloc(512);

    PRINT_DEBUG;

    _free(trd_mem);

    PRINT_DEBUG;

    if (fst_mem == NULL || snd_mem == NULL) {
        print_result(2, FAILURE, "Block(s) was freed although should not have been freed");
    }

    _free(fst_mem);
    _free(snd_mem);
    HEAP_DESTROY;
    print_result(2, SUCCESS, "OK");
}


void test_alloc_blocks_free_two() {
    void *heap = HEAP_INIT;

    if (heap == NULL) {
        print_result(3, FAILURE, "Could not initialize heap");
        return;
    }

    PRINT_DEBUG;

    void *fst_mem = _malloc(512);
    void *snd_mem = _malloc(512);
    void *trd_mem = _malloc(512);

    PRINT_DEBUG;

    _free(snd_mem);
    _free(trd_mem);

    PRINT_DEBUG;

    if (fst_mem == NULL) {
        print_result(3, FAILURE, "Block was freed although should not have been freed");
    }

    _free(fst_mem);
    HEAP_DESTROY;
    print_result(3, SUCCESS, "OK");
}


void test_new_region_extends_old() {
    struct block_header *heap = HEAP_INIT;

    if (heap == NULL) {
        print_result(4, FAILURE, "Could not initialize heap");
        return;
    }

    PRINT_DEBUG;

    void *mem = _malloc(REGION_MIN_SIZE * 2);

    if (mem == NULL) {
        print_result(4, FAILURE, "Could not allocate memory");
        HEAP_DESTROY;
        return;
    }

    PRINT_DEBUG;

    if (size_from_capacity((heap->capacity)).bytes < REGION_MIN_SIZE * 2) {
        print_result(4, FAILURE, "Heap was not extended");
    }

    _free(mem);
    heap_destroy(REGION_MIN_SIZE * 2, heap);
    print_result(4, SUCCESS, "OK");
}


void test_new_region_extends_old_another_place() {
    struct block_header *heap = HEAP_INIT;

    if (heap == NULL) {
        print_result(5, FAILURE, "Could not initialize heap");
        return;
    }

    PRINT_DEBUG;

    void *fst_mem = _malloc(REGION_MIN_SIZE);

    if (fst_mem == NULL) {
        print_result(5, FAILURE, "Could not allocate memory (first chunk)");
        HEAP_DESTROY;
        return;
    }

    PRINT_DEBUG;

    struct block_header *block = fst_mem - offsetof(struct block_header, contents);
    void *addr = block->capacity.bytes + block->contents;
    void *reg = mmap(
            addr,
            REGION_MIN_SIZE,
            PROT_READ | PROT_WRITE,
            MAP_PRIVATE | 0x20 | MAP_FIXED,
            -1,
            0
    );

    void *snd_mem = _malloc(REGION_MIN_SIZE);

    if (snd_mem == NULL) {
        print_result(5, FAILURE, "Could not allocate memory (second chunk)");
        _free(fst_mem);
        heap_destroy(REGION_MIN_SIZE, reg);
        HEAP_DESTROY;
        return;
    }

    PRINT_DEBUG;

    if (heap == addr) {
        print_result(5, FAILURE, "Invalid addressing");
    }

    PRINT_DEBUG;

    _free(snd_mem);
    _free(fst_mem);
    heap_destroy(REGION_MIN_SIZE, reg);
    HEAP_DESTROY;
    print_result(5, SUCCESS, "OK");
}


void test_all() {
    test_simple_mem_alloc();
    test_alloc_blocks_free_one();
    test_alloc_blocks_free_two();
    test_new_region_extends_old();
    test_new_region_extends_old_another_place();
}
